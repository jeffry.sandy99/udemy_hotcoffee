//
//  OrderViewModel.swift
//  Udemy_HotCoffee
//
//  Created by Jeffry Sandy Purnomo on 06/09/21.
//

import Foundation

class OrderListViewModel{
    var ordersViewModel: [OrderViewModel]
    
    init() {
        self.ordersViewModel = [OrderViewModel]()
    }
}

extension OrderListViewModel{
    func orderViewModel(at index: Int) -> OrderViewModel{
        return ordersViewModel[index]
    }
    
    func numberOfRows() -> Int{
        return ordersViewModel.count
    }
}

struct OrderViewModel{
    let order: Order
}

extension OrderViewModel{
    var name: String{
        return order.name
    }
    
    var coffeeName: String{
        return self.order.coffeeName.rawValue.capitalized
    }
    
    var size: String{
        return self.order.size.rawValue.capitalized
    }
    
    var total: Double{
        return order.total
    }
}
