//
//  AddCoffeeOrderViewModel.swift
//  Udemy_HotCoffee
//
//  Created by Jeffry Sandy Purnomo on 06/09/21.
//

import Foundation

struct AddCoffeeOrderViewModel{
    var name: String?
    var total: Double?
    var selectedType: String?
    var selectedSize: String?
    
    var coffeeName: [String]{
        return CoffeeName.allCases.map{$0.rawValue.capitalized}
    }
    
    var size: [String]{
        return CoffeeSize.allCases.map{$0.rawValue.capitalized}
    }
    
    var numberOfSection: Int{
        return AddCoffeeSection.allCases.count
    }
    
    var stepperTitle: String{
        return "Total"
    }
    
    var unknownTitle: String{
        return "Unknown"
    }
}

extension AddCoffeeOrderViewModel{
    func numberOfRow(at section: AddCoffeeSection) -> Int{
        switch section {
        case .coffeeNameOption:
            return coffeeName.count
        case .totalStepper:
            return 1
        }
    }
}
