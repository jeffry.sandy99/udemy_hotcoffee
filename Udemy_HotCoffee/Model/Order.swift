//
//  Order.swift
//  Udemy_HotCoffee
//
//  Created by Jeffry Sandy Purnomo on 06/09/21.
//

import Foundation

enum AddCoffeeSection: Int, CaseIterable{
    case coffeeNameOption = 0
    case totalStepper = 1
}

enum CoffeeName: String, Codable, CaseIterable{
    case cappuccino
    case latte
    case espressino
    case cortado
}

enum CoffeeSize: String, Codable, CaseIterable{
    case small
    case medium
    case large
}

struct Order: Codable{
    let name: String
    let coffeeName: CoffeeName
    let total: Double
    let size: CoffeeSize
}

extension Order{
    static var all: Resource<[Order]> = {
        guard let url = URL(string: "https://island-bramble.glitch.me/orders") else{
            fatalError("URL is incorrect ")
        }
        
        return Resource<[Order]>(url: url)
    }()
    
    init(_ vm: AddCoffeeOrderViewModel){
        guard let name = vm.name,
              let coffeeName = CoffeeName(rawValue: vm.selectedType!.lowercased()),
              let size = CoffeeSize(rawValue: vm.selectedSize!.lowercased()),
              let total = vm.total else {
            fatalError("Data not Fullfilled")
        }
        
        self.name = name
        self.coffeeName = coffeeName
        self.total = total
        self.size = size
    }
    
    static func create(_ vm: AddCoffeeOrderViewModel) -> Resource<Order>{
        let order = Order(vm)
        
        guard let data = try? JSONEncoder().encode(order) else {
            fatalError("Encoding order Error")
        }
        
        guard let url = URL(string: "https://island-bramble.glitch.me/orders") else{
            fatalError("URL is incorrect ")
        }
        
        var resource = Resource<Order>(url: url)
        resource.httpMethod = .post
        resource.body = data
        
        return resource
    }
}
