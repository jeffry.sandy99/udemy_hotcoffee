//
//  AddOrderViewController.swift
//  Udemy_HotCoffee
//
//  Created by Jeffry Sandy Purnomo on 02/09/21.
//

import Foundation
import UIKit

protocol AddCoffeeOrderDelegate {
    func addCoffeeOrderViewControllerDidSave(order: Order, controller: UIViewController)
    func addCoffeeOrderViewControllerDidClose(controller: UIViewController)
}

class AddOrderViewController: UIViewController{
    
    var delegate: AddCoffeeOrderDelegate?
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var nameTextField: UITextField!
    private var totalStepper: UIStepper!
    var addCoffeeViewModel = AddCoffeeOrderViewModel()
    private var coffeeSizeSegmentedControl: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI(){
        
        self.coffeeSizeSegmentedControl = UISegmentedControl(items: addCoffeeViewModel.size)
        self.coffeeSizeSegmentedControl.translatesAutoresizingMaskIntoConstraints = false
        
        self.totalStepper = UIStepper(frame: CGRect(x: 100, y: 200, width: 0, height: 0))
        self.totalStepper.translatesAutoresizingMaskIntoConstraints = false
        self.totalStepper.autorepeat = true
        self.totalStepper.minimumValue = 1
        self.totalStepper.maximumValue = 10
        self.totalStepper.stepValue = 1
        self.totalStepper.wraps = true
        self.totalStepper.addTarget(self, action: #selector(didTapStepper), for: .valueChanged)
        
        self.view.addSubview(coffeeSizeSegmentedControl)
        self.view.addSubview(totalStepper)
        
        NSLayoutConstraint.activate([
            self.coffeeSizeSegmentedControl.topAnchor.constraint(equalTo: tableView.bottomAnchor, constant: 20),
            self.coffeeSizeSegmentedControl.centerXAnchor.constraint(equalTo: view.centerXAnchor),
        ])
        
    }
    
    @objc func didTapStepper(){
        self.tableView.reloadData()
    }
    
    @IBAction func save(){
        let name = self.nameTextField.text
        let total = totalStepper.value
        
        let selectedSize = self.coffeeSizeSegmentedControl.titleForSegment(at: self.coffeeSizeSegmentedControl.selectedSegmentIndex)
        
        guard let indexPath = self.tableView.indexPathForSelectedRow else {
            fatalError("Error selecting coffee")
        }
        
        self.addCoffeeViewModel.name = name
        self.addCoffeeViewModel.total = total
        self.addCoffeeViewModel.selectedSize = selectedSize!
        self.addCoffeeViewModel.selectedType = self.addCoffeeViewModel.coffeeName[indexPath.row]
        
        WebService().load(resource: Order.create(addCoffeeViewModel)){ result in
            
            switch result{
            case .success(let order):
                if let delegate = self.delegate{
                    DispatchQueue.main.async {
                        delegate.addCoffeeOrderViewControllerDidSave(order: order, controller: self)
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
        
    }
    
    @IBAction func close(){
        if let delegate = self.delegate{
            delegate.addCoffeeOrderViewControllerDidClose(controller: self)
        }
    }
}

extension AddOrderViewController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return addCoffeeViewModel.numberOfSection
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addCoffeeViewModel.numberOfRow(at: AddCoffeeSection(rawValue: section) ?? .coffeeNameOption)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = AddCoffeeSection(rawValue: indexPath.section)
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddOrderViewController", for: indexPath)
        
        switch section {
        case .coffeeNameOption:
            cell.textLabel?.text = addCoffeeViewModel.coffeeName[indexPath.row]
            cell.detailTextLabel?.text = ""
        case .totalStepper:
            cell.textLabel?.text = addCoffeeViewModel.stepperTitle
            cell.detailTextLabel?.text = String(totalStepper.value)
            cell.accessoryView = totalStepper
        case .none:
            cell.textLabel?.text = addCoffeeViewModel.unknownTitle
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.accessoryType = .none
    }
}
